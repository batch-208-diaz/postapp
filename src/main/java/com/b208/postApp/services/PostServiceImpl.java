package com.b208.postApp.services;

import com.b208.postApp.config.JwtToken;
import com.b208.postApp.models.Post;
import com.b208.postApp.models.User;
import com.b208.postApp.repositories.PostRepository;
import com.b208.postApp.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class PostServiceImpl implements PostService {

    @Autowired
    private PostRepository postRepository;

    @Autowired
    JwtToken jwtToken;

    @Autowired
    private UserRepository userRepository;

    public void createPost(String stringToken,Post post) {

        User author = userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));

        //System.out.println(author.getUsername());
        Post newPost = new Post();
        newPost.setTitle(post.getTitle());
        newPost.setContent(post.getContent());
        newPost.setUser(author);

        postRepository.save(newPost);

    }

    public Iterable<Post> getPosts() {
        return postRepository.findAll();
    }

    public ResponseEntity updatePost(String stringToken, Long id, Post post) {

        Post postForUpdating = postRepository.findById(id).get();

        String postAuthor = postForUpdating.getUser().getUsername();
        String authenticatedUser = jwtToken.getUsernameFromToken(stringToken);

        //true if the user trying to update is the same as the author of the post
        //System.out.println(authenticatedUser.equals(postAuthor));

        if(authenticatedUser.equals(postAuthor)){
            postForUpdating.setTitle(post.getTitle());
            postForUpdating.setContent(post.getContent());
            postRepository.save(postForUpdating);
            return new ResponseEntity<>("Post Updated Successfully!",HttpStatus.OK);
        } else {
            return new ResponseEntity<>("You are not authorized to update this post.",HttpStatus.UNAUTHORIZED);
        }

        //System.out.println("This is the id passed as path variable.");
        //System.out.println(id);

        //System.out.println("This is the found data using the id.");
        //System.out.println(postForUpdating.getTitle());

        //System.out.println("This is the request body passed from the controller.");
        //System.out.println(post.getTitle());
    }

    public ResponseEntity deletePost(String stringToken,Long id){
        Post postForDeleting = postRepository.findById(id).get();
        String postAuthor = postForDeleting.getUser().getUsername();
        //System.out.println(postAuthor);

        String authenticatedUser = jwtToken.getUsernameFromToken(stringToken);
        if(authenticatedUser.equals(postAuthor)){
            postRepository.deleteById(id);
            return new ResponseEntity<>("Post Deleted Successfully!",HttpStatus.OK);
        } else {
            return new ResponseEntity<>("You are not authorized to delete this post.",HttpStatus.UNAUTHORIZED);
        }

    }

    /*
        - findById() retrieves a record that matches the passed id
        - Post post is from our request body from controller
        - converted to post class object, that's why it will get access to getTitle() method
     */

}
