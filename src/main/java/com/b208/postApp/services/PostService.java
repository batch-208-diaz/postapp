package com.b208.postApp.services;

import com.b208.postApp.models.Post;
import org.springframework.http.ResponseEntity;

public interface PostService {

    void createPost(String stringToken, Post post);
    Iterable<Post> getPosts();
    ResponseEntity updatePost(String stringToken, Long id, Post post);
    ResponseEntity deletePost(String stringToken, Long id);
}
