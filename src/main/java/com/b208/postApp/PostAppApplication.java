package com.b208.postApp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PostAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(PostAppApplication.class, args);
	}


}


/*

ODM (Object Document Mapping)
	-for NoSql ex. mongoose
ORM (Object Relational Mapping)
	- for relational database

Hibernate ORM
	- database credentials and hibernate dialect must be set in resources (applicatoin.properties file)
Models
	- class representation of a database entity, annotated with the @Entity annotation
Repositories
	- an abstraction of commonly-used functionalities performed on database objects. Extends the CrudRepository class.

@ com.b208.postApp/models/Post

	import javax.persistence.*; - add this import to consolidate all related persistence

	@Entity 			- mark this Java Object as a representation of a database table
	@Table 				- designate the table name that this model is connected to
	@Id 				- indicate that the following property is a primary key
	@GeneratedValue 	- auto-increment the id property
	@Column 			- class properties representing table columns in a relational database

	Constructors,Setters/Getters
		Default 		- needed when retrieving posts
		Parameterized 	- needed when creating posts

@ com.b208.postApp/repositories/PostRepository

	@Repository 		- an interface contains behaviours or methods that a class should contain
						- an interface marked as @Repository would contain method/s for database manipulation
	CrudRepository<>	- by extending CrudRepository, PostRepository has inherited pre-defined methods for CRUD

// S2 ACTIVITY - create a new model User with properties for username and password and create UserRepository

@ com.b208.postApp/services/PostService

	@Service			- added so springboot would recognize that business logic is implemented in this layer
	@Autowired 			- added when creating an instance of the postRepository that is persistent within our springboot app
						- to be able to use pre-defined methods for database manipulation for our table
	createPost			- a method to manipulate posts table (specific name only for this example)
						- when createPost is used from our services, it will be able to pass a Post class object
						- postRepository.save(post) - .save() method will insert a new row into posts table

@ com.b208.postApp/controllers/PostController

	@RestController		- handle all http responses
	@CrossOrigin		- enables cross origin resources requests
						- allow or disallow clients to access the API
						- CORS - cross origin resource sharing is the ability to allow or disallow applications to share resources from each other.

	@RequestMapping		- map web requests to controller methods
	@PostMapping		-
	@GetMapping			-
	ResponseEntity		- is an object that represents and contains
							- the whole HTTP response,
							- the actual message,
							- the status code.
	@RequestBody 		- automatically converts JSON client input into our desired object

	HttpStatus.created	- 201 in postman (look up http response status codes)
	HttpStatus.OK		- 200 OK in postman


	Process/How it travels from Postman to Repository:

	controller > service > service implementation > repository

	from RequestBody(JSON) 	-> Post post object in the controller
							-> postService
							-> .createPost(post)
							-> postRepository.save(post)


=== 8/22/2022 ============================================================================================================================================

//S4 discussion: JWT token / Authentication

@com.b208,postApp/repositories/UserRepository

	Custom Method -> "User findByUsername(String username)"
		- findBy* is an automatic and a convention for creating a customized query method via our CrudRepository

@com.b208,postApp/services/UserService

	Optional<User> findByUsername(String username)

@com.b208,postApp/services/UserServiceImpl

	Optional.ofNullable((userRepository.findByUsername(username)))
		- used in Registration to check users for validation

@com.b208,postApp/exceptions/UserException

	Exceptions - errors during runtime that stops the program

	UserException method that would show a message


//S5 discussion: Model

@com.b208,postApp/models/Post

	@ManyToOne and @JoinColumn(name="user_id",nullable = false)
		- we represent and tell Hibernate that this class represents the many side of the relationship
		- each post is linked to a user via the @JoinColumn, a reference using a foreign key to a user primary key
		- this column is also mapped by our user, that when the getPost() method of the User is called, all post rows, data belonging to the user will be gathered and returned
		- user_id will be the name of the foreign_key column

@com.b208,postApp/models/User

	@OneToMany(mappedBy = "user")
		- this will represent the one side of the relationship from "Post"
		- link this user to every post that have the same user_id to the primary key of the user
	@JsonIgnore
		- to prevent infinite recursion with bidirectional relationships
	private Set<Post> posts;
		- Set is used to have a unique collection of posts

 */