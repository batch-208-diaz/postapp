package com.b208.postApp.models;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name="posts")
public class Post {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private  Long id;

    @Column
    private String title;

    @Column
    private String content;


    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    /* Default Constructor */
    public Post(){}

    /* Parameterized Constructor */
    public Post(String title, String content){
        this.title = title;
        this.content = content;
    }

    /* Getters */
    public String getTitle(){
        return title;
    }

    public String getContent(){
        return content;
    }

    /* Setters */
    public void setTitle(String title){
        this.title = title;
    }

    public void setContent(String content){
        this.content = content;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
