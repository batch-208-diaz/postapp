package com.b208.postApp.repositories;
import com.b208.postApp.models.Post;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface PostRepository extends CrudRepository<Post,Object> {


}
