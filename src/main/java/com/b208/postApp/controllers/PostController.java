package com.b208.postApp.controllers;

import com.b208.postApp.models.Post;
import com.b208.postApp.services.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
public class PostController {

    @Autowired
    PostService postService;

    //@RequestMapping(value="/posts", method = RequestMethod.POST)
    /* to test first */
//    public void createPost(@RequestBody Post post){
//        System.out.println(post.getTitle());
//        System.out.println(post.getContent());
//


    @PostMapping("/posts")

//      public ResponseEntity<Object> createPost(@RequestBody Post post){
    //      postService.createPost(post);
    //      return new ResponseEntity<>("Post Created Successfully", HttpStatus.CREATED);
    //   }


    public ResponseEntity<Object> createPost(@RequestHeader(value="Authorization") String stringToken, @RequestBody Post post){

        postService.createPost(stringToken, post);
        return new ResponseEntity<>("Post Created Successfully", HttpStatus.CREATED);
    }

    @GetMapping("/posts")

    public ResponseEntity<Object> getPost(){
        return new ResponseEntity<>(postService.getPosts(), HttpStatus.OK);
    }

    @PutMapping("/posts/{id}")
    public ResponseEntity<Object> updatePost(@RequestHeader(value="Authorization") String stringToken, @PathVariable Long id, @RequestBody Post post){
    //public ResponseEntity<Object> updatePost(@PathVariable Long id, @RequestBody Post post){
        //System.out.println(id);
        //System.out.println(post);
        //System.out.println(post.getTitle());
        //System.out.println(post.getContent());
        //return new ResponseEntity<>("Post updated successfully.", HttpStatus.OK);
        return postService.updatePost(stringToken,id,post);
    }

    @DeleteMapping("/posts/{id}")
    public ResponseEntity<Object> deletePost(@RequestHeader(value="Authorization") String stringToken, @PathVariable Long id){

        //return new ResponseEntity<>("Post Deleted Successfully.", HttpStatus.OK);
        return postService.deletePost(stringToken,id);
    }
}
