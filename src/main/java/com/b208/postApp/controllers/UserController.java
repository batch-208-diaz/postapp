package com.b208.postApp.controllers;


import com.b208.postApp.exceptions.UserException;
import com.b208.postApp.models.User;
import com.b208.postApp.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@CrossOrigin
public class UserController {

    @Autowired
    UserService userService;

//    @PostMapping("/users")
//    public ResponseEntity<Object> createUser(@RequestBody User user){
//        userService.createUser(user);
//        return new ResponseEntity<>("User Created Successfully.", HttpStatus.CREATED);
//    }

    @PostMapping("/users/register")
    public ResponseEntity<Object> register(@RequestBody Map<String,String> body) throws UserException {

        String username = body.get("username");

        if(!userService.findByUsername(username).isEmpty()){
            throw new UserException("Username already exists.");
        } else {
            String password = body.get("password");
            String encodedPassword = new BCryptPasswordEncoder().encode(password);
            User newUser = new User(username,encodedPassword);
            userService.createUser(newUser);

            return new ResponseEntity<>("User Registered Successfully!",HttpStatus.CREATED);
        }
    }


    @GetMapping("/users")
    public ResponseEntity<Object> getUser(){
        return new ResponseEntity<>(userService.getUsers(), HttpStatus.OK);
    }

    @PutMapping("/users/{id}")
    public ResponseEntity<Object> updateUser(@PathVariable Long id, @RequestBody User user){
        userService.updateUser(id,user);
        return new ResponseEntity<>("User Updated successfully.", HttpStatus.OK);
    }

    @DeleteMapping("/users/{id}")
    public ResponseEntity<Object> deleteUser(@PathVariable Long id){
        userService.deleteUser(id);
        return new ResponseEntity<>("User Deleted Successfully.", HttpStatus.OK);
    }
}
